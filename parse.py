# encoding: utf-8
"""Example."""
import logging
try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup
import io
import os
import time


def main():
    """Main.

    We need to get the product information:
     - price
     - description
     - review score.
     """
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("Main.")
    
    dataset_file = io.open("dataset.txt", "wb")
    counter = 0
    counter_bad = 0
    counter_not_html = 0
    counter_soldout = 0
    logging.info("%s number of files processed out of %s", counter, len(os.listdir("data")))

    for filename in os.listdir("data"):
        if filename.endswith(".html"):
            logging.info(os.path.join("data", filename))
            with io.open(os.path.join("data", filename), mode='r', encoding="utf-8") as html:
                parsed_html = BeautifulSoup(html.read().encode('utf-8', 'ignore'),
                                            "lxml", from_encoding='utf-8')
                if not parsed_html.body:
                    continue
                price = parsed_html.body.find('span', attrs={'id': 'unit_price'})
                price = price.text if price else 0.0
                logging.info("price: %s", price)
                description = parsed_html.body.find('div', attrs={'class': 'pro_m_inner'})
                description = description.find('h1') if description else None
                description = description.text.encode('utf-8') if description else ''
                logging.info("description: %s", description)
                score = parsed_html.body.find('div', attrs={'class': 'pro_grade clearfix'})
                score = score.find('strong') if score else None
                score = score.text.encode('utf-8') if score else '0'
                logging.info("score: %s", score)
                # ul.choose_list > li.item > a > img
                img = parsed_html.body.find('ul', attrs={'class': 'choose_list'})
                img = img.find('li', attrs={'class': 'item'}) if img else None
                img = img.find('a') if img else None
                img = img.find('img') if img else None
                img = img['src'].encode('utf-8') if img else ''
                logging.info("img: %s", img)
                if 'en_soleout' in img:
                    counter_soldout += 1
                text = "{},{},{},{},{}\n".format(filename,
                                                 price,
                                                 score,
                                                 description,
                                                 img)
                dataset_file.write(text)
                counter += 1
                logging.info(counter)
    logging.info("%s number of files processed out of %s", counter, len(os.listdir("data")))
    logging.info("%s number of files sold out out of %s", counter_soldout, len(os.listdir("data")))
    dataset_file.close()


if __name__ == '__main__':
    main()
