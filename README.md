# Ilan's submission

##The results can be seen by:
1) Opening Analysis.html
2) Opening visualizations.html
3) Running interactiveClusters.R

## Code:
The main file is an ipython notebook called "Analysis.ipynb"
A second and third files are an R-markdown file called "visualizations.Rmd" and an R script called interactiveClusters.R

## Screenshot:
To see the interactive plot, run interactiveClusters.R within R studio.
![alt text](interactiveScreenshot.png "Interactive screenshot")
