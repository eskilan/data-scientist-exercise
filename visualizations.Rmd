---
title: "R Notebook"
output:
  html_document: default
  html_notebook: default
---

We use this R notebook to visualize our data. First, we load our library.
```{r results='hide', message=FALSE, warning=FALSE}
library(tidyverse)
```
Then, we load our data and split scores into categorical variables with "cut"

```{r results='hide', message=FALSE, warning=FALSE}
products <- read_csv('/home/ilan/Dropbox/Documents/job_search/tests/wideEyes/data-scientist-exercise/categorizedProducts.csv') %>% as_tibble()
# split scores into categories
products$scoreCat<-cut(products$Score, c(-1,0,5,10,20,50,1000))
summary(products)
```
Now, let's take a look at the number of items per category:
```{r}
# number of items of each class
ggplot(data = products) +
  geom_bar(mapping = aes(x=FinalCategory)) +
  theme(axis.text.x = element_text(angle = 60, hjust = 1)) +
  labs(x='Category',y='Number of items')
```

Now, let's look at the cost per category. We see that jackets, coats, etc. are the most expensive of all, while underwear and hats are generally inexpensive.
```{r}
ggplot(data = products) +
  geom_boxplot(mapping = aes(x=FinalCategory,y=Cost)) +
  theme(axis.text.x = element_text(angle = 60, hjust = 1)) +
  labs(x='Category',y='Cost')
```

This plot shows that there a relationship between cost and score in general.
```{r results='hide', message=FALSE, warning=FALSE}
# is there a relationship between cost and score in general?
ggplot(data = products,mapping = aes(x=Cost,y=Score)) +
  geom_point() + 
  geom_smooth(method = "lm") +
  scale_y_continuous(trans='log10')
```

We also see that many products in all categories have no review scores.
```{r}
# looking at correlations between score and category
ggplot(data=products) + geom_count(mapping = aes(x=FinalCategory,y=scoreCat)) + 
  theme(axis.text.x = element_text(angle = 60, hjust = 1)) 
```

The following tile plot shows that in some categories cheaper is highly rated (as in hats) but in others such as jackets (expensive can be associated with higher score):
```{r}
# median cost in bins defined by score and category
productsByMedianCost <- products %>% group_by(FinalCategory,scoreCat) %>% summarise(medianCost = median(Cost))
ggplot(data=productsByMedianCost) + geom_tile(mapping = aes(x=FinalCategory,y=scoreCat,fill=medianCost)) + 
  theme(axis.text.x = element_text(angle = 60, hjust = 1)) 
```


